import 'package:flutter/material.dart';
import 'package:trelloapps/BoardScreen/BoardScreen.dart';
import 'package:trelloapps/Home/BoardPage.dart';
import 'package:trelloapps/Provider/auth_provider.dart';
import 'package:trelloapps/RegistAndLogin/Register.dart';
import 'package:trelloapps/RegistAndLogin/Reset.dart';

class LoginPage extends StatefulWidget {
  // const LoginPage({ Key? key }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  bool _isLoading = false;
  bool _isVisible = false;
  bool _canshowButton = true;
  bool _isHidePassword = true;
  void hideWidget() {
    setState(() {
      _canshowButton = !_canshowButton;
    });
  }

  @override
  Widget build(BuildContext context) {
    double layoutHeight = MediaQuery.of(context).size.height;
    double layoutWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.blue.shade600,
      body: Container(
        height: layoutHeight,
        width: layoutWidth,
        child: Column(
          children: <Widget>[
            Container(
              height: layoutHeight * 0.7,
              width: layoutWidth,
              child: _isLoading == false
                  ? Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 50),
                        child: Column(
                          children: [
                            TextFormField(
                              showCursor: true,
                              cursorColor: Colors.white,
                              controller: _email,
                              decoration: InputDecoration(
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                labelText: 'Email',
                                labelStyle: TextStyle(color: Colors.white),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 290),
                              child: !_canshowButton
                                  ? const SizedBox.shrink()
                                  : FlatButton(
                                      color: Colors.green,
                                      onPressed: () {
                                        hideWidget();
                                        setState(() {
                                          _isVisible = !_isVisible;
                                        });
                                      },
                                      child: Text("NEXT"),
                                    ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Visibility(
                              visible: _isVisible,
                              child: Column(
                                children: [
                                  TextFormField(
                                    obscureText: _isHidePassword,
                                    cursorColor: Colors.white,
                                    controller: _password,
                                    decoration: InputDecoration(
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.white)),
                                        labelText: 'Password',
                                        suffixIcon: IconButton(
                                            color: Colors.white,
                                            onPressed: () {
                                              setState(() {
                                                _isHidePassword =
                                                    !_isHidePassword;
                                              });
                                            },
                                            icon: Icon(_isHidePassword
                                                ? Icons.visibility_off
                                                : Icons.visibility)),
                                        labelStyle:
                                            TextStyle(color: Colors.white)),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 195),
                                    child: Row(
                                      children: [
                                        FlatButton(
                                            onPressed: () {
                                              Navigator.pushAndRemoveUntil(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          Board()),
                                                  (route) => false);
                                            },
                                            child: Text("Cancle")),
                                        FlatButton(
                                            color: Colors.green,
                                            onPressed: () {
                                              setState(() {
                                                _isLoading = true;
                                              });
                                              AuthClass()
                                                  .SignIn(
                                                      email: _email.text.trim(),
                                                      password:
                                                          _password.text.trim())
                                                  .then((value) {
                                                if (value == "Welcome") {
                                                  setState(() {
                                                    _isLoading = false;
                                                  });
                                                  Navigator.pushAndRemoveUntil(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              BoardPage()),
                                                      (route) => false);
                                                } else {
                                                  setState(() {
                                                    _isLoading = false;
                                                  });
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(SnackBar(
                                                          content:
                                                              Text(value)));
                                                }
                                              });
                                            },
                                            child: Text('Log In')),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Register()),
                                    (route) => false);
                              },
                              child: Text("Belum punya akun? Daftar"),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ResetPage()));
                              },
                              child: Text("Lupa Password? Reset"),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ),
                    )
                  : Center(
                      child: CircularProgressIndicator(),
                    ),
            ),
            Expanded(
              child: Scaffold(
                body: Container(
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Image.asset(
                              "images/google.png",
                              height: 30,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "SIGN IN WITH GOOGLE",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Image.asset(
                              "images/microsoft.png",
                              height: 30,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "SIGN IN WITH MICROSOFT",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Image.asset(
                              "images/apple.png",
                              height: 30,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "SIGN IN WITH APPLE",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Image.asset(
                              "images/slack.png",
                              height: 30,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "SIGN IN WITH SLACK",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
