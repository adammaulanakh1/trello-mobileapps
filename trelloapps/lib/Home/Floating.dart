import 'package:flutter/material.dart';
import 'dart:math' as math;

class MaterialHome extends StatelessWidget {
  // const MaterialHome({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: AnimatingIconsHere(),
        ),
      ),
    );
  }
}

class AnimatingIconsHere extends StatefulWidget {
  // const AnimatingIconsHere({ Key? key }) : super(key: key);

  @override
  _AnimatingIconsHereState createState() => _AnimatingIconsHereState();
}

class _AnimatingIconsHereState extends State<AnimatingIconsHere>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation animation;

  Icon presenticon = Icon(
    Icons.add,
    size: 40,
  );

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(seconds: 2));
    animation = Tween(begin: 0.0, end: math.pi / 2).animate(
        CurvedAnimation(parent: controller, curve: Curves.bounceInOut));
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        height: 100,
        width: 100,
        decoration: BoxDecoration(
          color: Colors.tealAccent,
          borderRadius: BorderRadius.circular(30),
        ),
        child: AnimatedBuilder(
          animation: animation,
          builder: (context, child) {
            return Transform.rotate(
              angle: 180 * math.pi / 180,
              child: presenticon,
            );
          },
        ),
      ),
      onTap: () {
        if (animation.status == AnimationStatus.completed) {
          controller.reverse();
          presenticon = Icon(
            Icons.add,
            size: 40,
          );
        } else if (animation.status == AnimationStatus.dismissed) {
          controller.forward();
          presenticon = Icon(
            Icons.remove,
            size: 40,
          );
        }
      },
    );
  }
}
