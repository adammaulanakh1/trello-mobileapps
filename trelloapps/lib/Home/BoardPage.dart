import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:trelloapps/Home/AddBoard.dart';
import 'package:trelloapps/Home/AddCard.dart';
import 'package:trelloapps/Home/BoardFix.dart';
import 'package:trelloapps/Home/EditBoard.dart';
import 'package:trelloapps/Home/NavBar.dart';
import 'package:trelloapps/reorderable/reorderable.dart';

class BoardPage extends StatefulWidget {
  // const BoardPage({ Key? key }) : super(key: key);
  String user = FirebaseAuth.instance.currentUser.email;
  @override
  _BoardPageState createState() => _BoardPageState();
}

class _BoardPageState extends State<BoardPage> {
  // StringP user = FirebaseAuth.instance.currentUser.email;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBar(),
      appBar: AppBar(
        backgroundColor: Colors.blue.shade600,
        title: Text("Boards"),
        actions: [
          Row(
            children: [
              IconButton(onPressed: () {}, icon: Icon(Icons.search)),
              IconButton(
                  icon: Icon(Icons.notifications_outlined), onPressed: () {}),
            ],
          )
        ],
      ),
      body: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            "Training",
            style: TextStyle(
                color: Colors.black,
                fontSize: 15,
                fontWeight: FontWeight.normal),
          ),
        ),
        body: Stack(
          children: [
            StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection("board")
                    // .where("email", isEqualTo: widget.user)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (!snapshot.hasData)
                    return new Container(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  return BoardList(document: snapshot.data.docs);
                })
          ],
        ),
      ),
      floatingActionButton: SpeedDial(
        curve: Curves.bounceInOut,
        animatedIcon: AnimatedIcons.add_event,
        children: [
          SpeedDialChild(
              child: Icon(Icons.home),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => AddBoard()),
                    (route) => false);
              },
              label: "Boards"),
          SpeedDialChild(
              child: Icon(Icons.card_travel_sharp),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => AddCard()),
                    (route) => false);
              },
              label: "Cards"),
        ],
      ),
    );
  }
}

class BoardList extends StatelessWidget {
  // const BoardList({ Key? key }) : super(key: key);
  BoardList({this.document});
  final List<DocumentSnapshot> document;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: document.length,
      itemBuilder: (BuildContext context, int i) {
        String board = document[i].data().toString();
        return Dismissible(
          key: Key(document[i].id),
          onDismissed: (direction) {
            FirebaseFirestore.instance.runTransaction((transaction) async {
              DocumentSnapshot snapshot =
                  await transaction.get(document[i].reference);
              await transaction.delete(snapshot.reference);
            });
            Scaffold.of(context)
                .showSnackBar(new SnackBar(content: Text("Data dihapus")));
          },
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 10.0, top: 8.0, right: 16.0, bottom: 8.0),
                          child: Row(
                            children: [
                              Container(
                                height: 50,
                                width: 50,
                                color: Colors.blue.shade600,
                              ),
                              Container(
                                height: 50,
                                width: 250,
                                child: Card(
                                  semanticContainer: true,
                                  margin: EdgeInsets.all(5),
                                  elevation: 5,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  BoardCardView()
                                              // KanbanDemo()
                                              // BoardView(
                                              //   board: board,
                                              //   index: document[i].reference,
                                              // ),
                                              ),
                                        );
                                      },
                                      child: Text(
                                        board,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.normal),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              IconButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => EditDataBoard(
                                                  board: board,
                                                  index: document[i].reference,
                                                )));
                                  },
                                  icon: Icon(Icons.edit))
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
