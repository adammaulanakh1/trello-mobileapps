import 'package:flutter/material.dart';
import 'package:trelloapps/Home/NavBar.dart';

class Workspace extends StatefulWidget {
  // const Workspace({ Key? key }) : super(key: key);

  @override
  _WorkspaceState createState() => _WorkspaceState();
}

class _WorkspaceState extends State<Workspace> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue.shade600,
      drawer: NavBar(),
      appBar: AppBar(
        title: Text("Workspace"),
        actions: [
          Row(
            children: [
              IconButton(onPressed: () {}, icon: Icon(Icons.search)),
              IconButton(
                  icon: Icon(Icons.notifications_outlined), onPressed: () {}),
            ],
          )
        ],
      ),
      body: Scaffold(
        body: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              title: TabBar(
                indicatorColor: Colors.blue,
                labelColor: Colors.blue,
                tabs: [
                  Tab(
                    child: Text(
                      "BOARDS",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "HIGHLIGHTS",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
            body: TabBarView(children: [
              Card(
                color: Colors.grey.shade200,
                child: Column(
                  children: [
                    Container(
                      color: Colors.white,
                      height: 30,
                      width: 500,
                      child: Center(
                          child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text("Your Workspace boards")),
                      )),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        children: [
                          Container(
                            color: Colors.blue,
                            height: 100,
                            width: 130,
                          ),
                          Container(
                              child: Container(
                                  height: 30,
                                  width: 130,
                                  color: Colors.blue.shade700,
                                  child: Center(
                                    child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              "Untitled",
                                              style: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ))),
                                  )))
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Card(
                child: Text("Tab2"),
              )
            ]),
          ),
        ),
      ),
    );
  }
}
