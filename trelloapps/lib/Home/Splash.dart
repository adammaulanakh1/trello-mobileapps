import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:trelloapps/BoardScreen/BoardScreen.dart';
import 'package:trelloapps/Home/BoardPage.dart';
import 'package:trelloapps/RegistAndLogin/Login.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  FirebaseAuth auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 2), () {
      if (auth.currentUser == null) {
        Navigator.pushAndRemoveUntil(context,
            MaterialPageRoute(builder: (context) => Board()), (route) => false);
      } else {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => BoardPage()),
            (route) => false);
      }
    });
    return Scaffold(
      body: Container(
        child: Center(
          child: Container(
              child: Image.asset(
            'images/logo_trello_white.png',
            height: 300,
            width: 300,
          )),
        ),
        color: Colors.blue.shade500,
      ),
    );
  }
}
