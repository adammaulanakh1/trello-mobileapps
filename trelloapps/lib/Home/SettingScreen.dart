import 'package:flutter/material.dart';
import 'package:trelloapps/BoardScreen/BoardScreen.dart';
import 'package:trelloapps/BoardScreen/Welcome.dart';
import 'package:trelloapps/Provider/auth_provider.dart';

class SettingScreen extends StatefulWidget {
  // const SettingScreen({ Key? key }) : super(key: key);

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  var chackBoxValue = false;
  var chackBoxValue1 = true;
  var chackBoxValue2 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue.shade600,
        title: Text("Settings"),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Stack(
          children: <Widget>[
            Container(
                child: Column(
              children: [
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 58),
                          child: Text(
                            "Application theme",
                            style: TextStyle(
                                color: Colors.green,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 65),
                          child: Text("Select Theme"),
                        )),
                  ],
                ),
                Column(),
                Divider(
                  thickness: 2,
                ),
                SizedBox(
                  height: 15,
                ),
                Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 65),
                        child: Text(
                          "Accessibility",
                          style: TextStyle(
                              color: Colors.green, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  SizedBox(
                                    width: 60,
                                  ),
                                  Text("Color blind friendly mode"),
                                  SizedBox(
                                    width: 75,
                                  ),
                                  Checkbox(
                                      activeColor: Colors.green,
                                      value: chackBoxValue,
                                      onChanged: (newValue) {
                                        setState(() {
                                          chackBoxValue = !chackBoxValue;
                                        });
                                      })
                                ],
                              ),
                              Row(
                                children: [
                                  SizedBox(
                                    width: 60,
                                  ),
                                  Text("Enable animatios"),
                                  SizedBox(
                                    width: 123,
                                  ),
                                  Checkbox(
                                      activeColor: Colors.green,
                                      value: chackBoxValue1,
                                      onChanged: (newValue) {
                                        setState(() {
                                          chackBoxValue1 = !chackBoxValue1;
                                        });
                                      })
                                ],
                              ),
                              Row(
                                children: [
                                  SizedBox(
                                    width: 60,
                                  ),
                                  Text("Show label names on card"),
                                  SizedBox(
                                    width: 67,
                                  ),
                                  Checkbox(
                                      activeColor: Colors.green,
                                      value: chackBoxValue2,
                                      onChanged: (newValue) {
                                        setState(() {
                                          chackBoxValue2 = !chackBoxValue2;
                                        });
                                      })
                                ],
                              ),
                              Divider(
                                thickness: 2,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Sync",
                                    style: TextStyle(
                                        color: Colors.green,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Text(
                                    "Offline boards\nSelect board for offline viewing and editing",
                                    maxLines: 2,
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Text("Sync queue")
                                ],
                              ),
                              Divider(
                                thickness: 2,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "General",
                                      style: TextStyle(
                                          color: Colors.green,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Text("Profile and visibility"),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Text("Create card defaults"),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Text("Disconnect Google"),
                                    Text(
                                      "Revoke any permissions you've granted\nTrello to access your Goolge data",
                                      maxLines: 2,
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Text("Delete account"),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Text("About Trello"),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Text("Report a bug"),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    GestureDetector(
                                      child: Text("Log out"),
                                      onTap: () {
                                        AuthClass().signOut();
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => Board()));
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            )),
          ],
        ),
      ),
    );
  }

//   _checkBox() {
//     return Checkbox(
//         activeColor: Colors.green,
//         value: chackBoxValue,
//         onChanged: (newValue) {
//           setState(() {
//             chackBoxValue = !chackBoxValue;
//           });
//         });
//   }
}
