import 'package:flutter/material.dart';
import 'package:trelloapps/BoardScreen/DragBoardCard.dart';
import 'package:trelloapps/Home/BoardPage.dart';
import 'package:trelloapps/Home/CardPage.dart';
import 'package:trelloapps/Home/HelpPage.dart';
import 'package:trelloapps/Home/Menu.dart';
import 'package:trelloapps/Home/SettingScreen.dart';
import 'package:trelloapps/Home/Workspace.dart';

class MyHome extends StatefulWidget {
  // const Home({ Key? key }) : super(key: key);

  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  // const HomePage({ Key? key }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var currentPage = DrawerSections.boards;
  @override
  Widget build(BuildContext context) {
    var container;
    if (currentPage == DrawerSections.boards) {
      container = BoardPage();
    } else if (currentPage == DrawerSections.home) {
      container = DragBoardCard();
    } else if (currentPage == DrawerSections.training) {
      container = Workspace();
    } else if (currentPage == DrawerSections.mycard) {
      container = CardPage();
    } else if (currentPage == DrawerSections.settings) {
      container = SettingScreen();
    } else if (currentPage == DrawerSections.help) {
      container = HelpPage();
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Boards"),
      ),
      body: container,
      drawer: Drawer(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Menu(),
                drawerList(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget drawerList() {
    return Container(
      padding: EdgeInsets.only(
        top: 15,
      ),
      child: Column(
        children: [
          menuItem(1, "Boards", Icons.dashboard,
              currentPage == DrawerSections.boards ? true : false),
          menuItem(2, "Home", Icons.home,
              currentPage == DrawerSections.home ? true : false),
          menuItem(3, "Training", Icons.dashboard,
              currentPage == DrawerSections.training ? true : false),
          menuItem(4, "My cards", Icons.card_membership,
              currentPage == DrawerSections.mycard ? true : false),
          menuItem(5, "Settings", Icons.settings,
              currentPage == DrawerSections.settings ? true : false),
          menuItem(6, "Help!", Icons.info_outline,
              currentPage == DrawerSections.help ? true : false),
        ],
      ),
    );
  }

  Widget menuItem(int id, String title, IconData icon, bool selected) {
    return Material(
      color: selected ? Colors.blue.shade100 : Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.pop(context);
          setState(() {
            if (id == 1) {
              currentPage = DrawerSections.boards;
            } else if (id == 2) {
              currentPage = DrawerSections.home;
            } else if (id == 3) {
              currentPage = DrawerSections.training;
            } else if (id == 4) {
              currentPage = DrawerSections.mycard;
            } else if (id == 5) {
              currentPage = DrawerSections.settings;
            } else if (id == 6) {
              currentPage = DrawerSections.help;
            }
          });
        },
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Row(
            children: [
              Expanded(
                child: Icon(
                  icon,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                flex: 5,
                child: Text(
                  title,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

enum DrawerSections {
  boards,
  home,
  workspace,
  training,
  mycard,
  settings,
  help
}
