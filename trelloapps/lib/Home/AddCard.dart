import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:trelloapps/Home/BoardPage.dart';
import 'package:trelloapps/Home/CardPage.dart';

class AddCard extends StatefulWidget {
  // const AddCard({ Key? key }) : super(key: key);
  final String user = FirebaseAuth.instance.currentUser.email;
  @override
  _AddCardState createState() => _AddCardState();
}

class _AddCardState extends State<AddCard> {
  String valueChoose;
  List listItem = ["learn php"];
  DateTime _dueDate = new DateTime.now();
  String _dateText = '';
  String _dateText1 = '';
  String card = '';
  String description = '';

  Future<Null> _selectDueDate(BuildContext context) async {
    final picked = await showDatePicker(
      context: context,
      initialDate: _dueDate,
      firstDate: DateTime(1999),
      lastDate: DateTime(2024),
    );
    if (picked != null) {
      setState(() {
        _dueDate = picked;
        _dateText = "${picked.day}/${picked.month}/${picked.year}";
      });
    }
  }

  Future<Null> _selectDueDate1(BuildContext context) async {
    final picked = await showDatePicker(
      context: context,
      initialDate: _dueDate,
      firstDate: DateTime(1999),
      lastDate: DateTime(2024),
    );
    if (picked != null) {
      setState(() {
        _dueDate = picked;
        _dateText1 = "${picked.day}/${picked.month}/${picked.year}";
      });
    }
  }

  void _addData() {
    FirebaseFirestore.instance.runTransaction((Transaction transaction) async {
      CollectionReference reference =
          FirebaseFirestore.instance.collection('card');
      await reference.add({
        "email ": widget.user,
        "card": card,
        "description": description,
        "startdate": _dateText,
        "duedate": _dateText1,
      });
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => BoardPage()),
          (route) => false);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dateText = "Start date...";
    _dateText1 = "Due date...";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => BoardPage()),
                (route) => false);
          },
          icon: Icon(Icons.close),
        ),
        title: Text("Create Card"),
        actions: [
          IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                _addData();
              }),
        ],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text("Board"),
                    ),
                    DropdownButton(
                      hint: Text('learn php'),
                      isExpanded: true,
                      value: valueChoose,
                      onChanged: (newValue) {
                        setState(() {
                          valueChoose = newValue;
                        });
                      },
                      items: listItem.map((valueItem) {
                        return DropdownMenuItem(
                          value: valueItem,
                          child: Text(valueItem),
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text("List"),
                    ),
                    DropdownButton(
                      hint: Text('learn php'),
                      isExpanded: true,
                      value: valueChoose,
                      onChanged: (newValue) {
                        setState(() {
                          valueChoose = newValue;
                        });
                      },
                      items: listItem.map((valueItem) {
                        return DropdownMenuItem(
                          value: valueItem,
                          child: Text(valueItem),
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
              Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(8.0),
                    height: 290,
                    decoration: BoxDecoration(color: Colors.blue),
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 15, 10, 10),
                        child: Container(
                          height: 260,
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Column(
                              children: [
                                TextFormField(
                                  onChanged: (String str) {
                                    setState(() {
                                      card = str;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Card name',
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                TextFormField(
                                  onChanged: (String str) {
                                    setState(() {
                                      description = str;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Description',
                                  ),
                                ),
                                Column(
                                  children: [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Icon(Icons.timelapse_outlined),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        GestureDetector(
                                          onTap: () => _selectDueDate(context),
                                          child: Text(_dateText),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        SizedBox(
                                          width: 35,
                                        ),
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: GestureDetector(
                                            onTap: () =>
                                                _selectDueDate1(context),
                                            child: Text(_dateText1),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
