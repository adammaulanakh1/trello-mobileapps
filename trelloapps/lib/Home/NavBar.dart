import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:trelloapps/BoardScreen/DragBoardCard.dart';
import 'package:trelloapps/Home/BoardPage.dart';
import 'package:trelloapps/Home/CardPage.dart';
import 'package:trelloapps/Home/SettingScreen.dart';
import 'package:trelloapps/Home/Workspace.dart';

class NavBar extends StatefulWidget {
  // const NavBar({ Key? key }) : super(key: key);
  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  int _selected = 0;
  void changeSelected(int index) {
    setState(() {
      _selected = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    String user = FirebaseAuth.instance.currentUser.email;
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text("Adam"),
            accountEmail: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("$user"),
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.arrow_drop_down_sharp),
                      iconSize: 30,
                    )
                  ],
                ),
              ],
            ),
            currentAccountPicture: CircleAvatar(
              child: Image.asset(
                "images/logo_trello.png",
                fit: BoxFit.cover,
              ),
            ),
          ),
          ListTile(
            selected: _selected == 0,
            leading: Icon(
              Icons.departure_board,
            ),
            title: Text("Boards"),
            onTap: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => BoardPage()),
                  (route) => false);
              changeSelected(0);
            },
          ),
          ListTile(
            selected: _selected == 1,
            leading: Icon(Icons.home),
            title: Row(
              children: [
                Text("Home"),
              ],
            ),
            onTap: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => DragBoardCard()),
                  (route) => false);
              changeSelected(1);
            },
          ),
          Divider(
            thickness: 2,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Workspaces",
            ),
          ),
          ListTile(
            selected: _selected == 2,
            leading: Icon(
              Icons.people_outline,
            ),
            title: Text("learn php"),
            onTap: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => Workspace()),
                  (route) => false);
              changeSelected(2);
            },
          ),
          Divider(
            thickness: 2,
          ),
          ListTile(
            selected: _selected == 3,
            leading: Icon(
              Icons.card_giftcard,
            ),
            title: Text("My cards"),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => CardPage()),
              );
              changeSelected(3);
            },
          ),
          ListTile(
            selected: _selected == 4,
            leading: Icon(
              Icons.settings,
            ),
            title: Text("Settings"),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SettingScreen()));
              changeSelected(4);
            },
          ),
          ListTile(
            selected: _selected == 5,
            leading: Icon(Icons.info_outline),
            title: Text("Help!"),
            onTap: () {
              changeSelected(5);
            },
          ),
        ],
      ),
    );
  }
}

class Entry {
  final String title;
  final List<Entry> children;
  Entry(this.title, [this.children = const <Entry>[]]);
}

final List<Entry> data = <Entry>[
  Entry(
    'Add Account',
    <Entry>[
      Entry('Section A1', <Entry>[
        Entry('Account 1'),
        Entry('Account 2'),
        Entry('Account 3'),
      ])
    ],
  )
];

class EntryItem extends StatelessWidget {
  // const EnrtyItem({ Key? key }) : super(key: key);
  const EntryItem(this.entry);
  final Entry entry;

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty) {
      return ListTile(
        title: Text(root.title),
      );
    }
    return ExpansionTile(
      key: PageStorageKey<Entry>(root),
      title: Text(root.title),
      children: root.children.map<Widget>(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}
