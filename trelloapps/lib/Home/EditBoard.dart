import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:trelloapps/BoardScreen/BoardScreen.dart';
import 'package:trelloapps/Home/AddBoard.dart';
import 'package:trelloapps/Home/BoardPage.dart';

class EditDataBoard extends StatefulWidget {
  EditDataBoard({this.board, this.index});
  String board;
  final index;
  // const EditDataBoard({ Key? key }) : super(key: key);
  final String user = FirebaseAuth.instance.currentUser.email;
  @override
  _EditDataBoardState createState() => _EditDataBoardState();
}

String valueChoose;
String valueChoose1;
List listItem = ["learn php"];
List listItem1 = ["Private"];

String board;
String workspace;
String visibilty;

class _EditDataBoardState extends State<EditDataBoard> {
  TextEditingController controllerboard;
  void _EditBoard() {
    FirebaseFirestore.instance.runTransaction((Transaction transaction) async {
      DocumentSnapshot snapshot = await transaction.get(widget.index);
      await transaction.update(snapshot.reference, {
        "board": board,
      });
    });
    Navigator.pop(context);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    board = widget.board;
    controllerboard = new TextEditingController(text: widget.board);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue.shade600,
        leading: IconButton(
          onPressed: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => BoardPage()),
                (route) => false);
          },
          icon: Icon(Icons.close),
        ),
        title: Text("Edit board"),
        actions: [
          IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                _EditBoard();
              })
        ],
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              TextFormField(
                controller: controllerboard,
                onChanged: (String str) {
                  setState(() {
                    board = str;
                  });
                },
                showCursor: true,
                cursorColor: Colors.green,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.green),
                    ),
                    labelText: 'Board name',
                    labelStyle: TextStyle(color: Colors.green)),
              ),
              SizedBox(
                height: 15,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text("Workspace"),
              ),
              DropdownButton(
                hint: Text('learn php'),
                isExpanded: true,
                value: valueChoose,
                onChanged: (newValue) {
                  setState(() {
                    valueChoose = newValue;
                  });
                },
                items: listItem.map((valueItem) {
                  return DropdownMenuItem(
                    value: valueItem,
                    child: Text(valueItem),
                  );
                }).toList(),
              ),
              SizedBox(
                height: 15,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text("Visibility"),
              ),
              DropdownButton(
                hint: Text('Workspace'),
                isExpanded: true,
                value: valueChoose1,
                onChanged: (newValue1) {
                  setState(() {
                    valueChoose1 = newValue1;
                  });
                },
                items: listItem1.map((valueItem1) {
                  return DropdownMenuItem(
                    value: valueItem1,
                    child: Row(
                      children: [
                        Icon(Icons.lock),
                        SizedBox(
                          height: 5,
                        ),
                        Text(valueItem1),
                      ],
                    ),
                  );
                }).toList(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
