import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:trelloapps/Home/BoardPage.dart';

class AddBoard extends StatefulWidget {
  AddBoard({this.email});
  String email;
  // const AddBoard({ Key? key }) : super(key: key);
  final String user = FirebaseAuth.instance.currentUser.email;
  @override
  _AddBoardState createState() => _AddBoardState();
}

String valueChoose;
String valueChoose1;
List listItem = ["learn php"];
List listItem1 = ["Private"];

String board = '';
String workspace = '';
String visibilty = '';

class _AddBoardState extends State<AddBoard> {
  void _addData() {
    FirebaseFirestore.instance.runTransaction((Transaction transaction) async {
      CollectionReference reference =
          FirebaseFirestore.instance.collection('board');
      await reference.add({
        // "email": widget.user,
        "board": board,
      });
    });
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context) => BoardPage()), (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue.shade600,
        leading: IconButton(
          onPressed: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => BoardPage()),
                (route) => false);
          },
          icon: Icon(Icons.close),
        ),
        title: Text("Create board"),
        actions: [
          IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                _addData();
              })
        ],
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              TextFormField(
                onChanged: (String str) {
                  setState(() {
                    board = str;
                  });
                },
                showCursor: true,
                cursorColor: Colors.green,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.green),
                    ),
                    labelText: 'Board name',
                    labelStyle: TextStyle(color: Colors.green)),
              ),
              SizedBox(
                height: 15,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text("Workspace"),
              ),
              DropdownButton(
                hint: Text('learn php'),
                isExpanded: true,
                value: valueChoose,
                onChanged: (newValue) {
                  setState(() {
                    valueChoose = newValue;
                  });
                },
                items: listItem.map((valueItem) {
                  return DropdownMenuItem(
                    value: valueItem,
                    child: Text(valueItem),
                  );
                }).toList(),
              ),
              SizedBox(
                height: 15,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text("Visibility"),
              ),
              DropdownButton(
                hint: Text('Workspace'),
                isExpanded: true,
                value: valueChoose1,
                onChanged: (newValue1) {
                  setState(() {
                    valueChoose1 = newValue1;
                  });
                },
                items: listItem1.map((valueItem1) {
                  return DropdownMenuItem(
                    value: valueItem1,
                    child: Row(
                      children: [
                        Icon(Icons.lock),
                        SizedBox(
                          height: 5,
                        ),
                        Text(valueItem1),
                      ],
                    ),
                  );
                }).toList(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
