import 'package:flutter/material.dart';
import 'package:trelloapps/Home/AddBoard.dart';

class BoardView extends StatefulWidget {
  BoardView({this.board, this.index});
  String board;
  final index;
  // const BoardView({ Key? key }) : super(key: key);

  @override
  _BoardViewState createState() => _BoardViewState();
}

class _BoardViewState extends State<BoardView> {
  TextEditingController controllerboard;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    board = widget.board;
    controllerboard = new TextEditingController(text: widget.board);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.board),
        actions: [
          Row(
            children: [
              IconButton(
                  onPressed: () {}, icon: Icon(Icons.filter_alt_outlined)),
              IconButton(
                  onPressed: () {}, icon: Icon(Icons.notifications_outlined)),
              IconButton(icon: Icon(Icons.more_horiz), onPressed: () {}),
            ],
          )
        ],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: BoardList(),
      ),
    );
  }
}

class BoardList extends StatefulWidget {
  // const BoardList({ Key? key }) : super(key: key);

  @override
  _BoardListState createState() => _BoardListState();
}

class _BoardListState extends State<BoardList> {
  bool _isVisible = false;
  bool _canshowButton = true;
  void hideWidget() {
    setState(() {
      _canshowButton = !_canshowButton;
    });
  }

  List<String> myCustomList = [
    "card 1",
    "card 2",
    "card 3",
    "card 1",
    "card 2",
    "card 3"
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Draggable<String>(
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.grey.shade300,
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              height: 320,
              width: 210,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("TODO"),
                            IconButton(
                                onPressed: () {}, icon: Icon(Icons.more_vert))
                          ],
                        ),
                        Visibility(
                          visible: _isVisible,
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextFormField(
                                showCursor: true,
                                cursorColor: Colors.black,
                                decoration: InputDecoration(
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white)),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 130,
                    child: ReorderableListView(
                      children: List.generate(
                        myCustomList.length,
                        (index) {
                          return ListTile(
                            key: UniqueKey(),
                            title: Card(
                                child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(myCustomList[index]),
                            )),
                          );
                        },
                      ),
                      onReorder: (int oldIndex, int newIndex) {
                        setState(() {
                          if (newIndex > oldIndex) {
                            newIndex -= 1;
                          }
                          final String newString =
                              myCustomList.removeAt(oldIndex);
                          myCustomList.insert(newIndex, newString);
                        });
                      },
                    ),
                  ),
                  Row(
                    children: [
                      Visibility(
                        visible: _isVisible,
                        child: Row(
                          children: [
                            IconButton(
                              onPressed: () {},
                              icon: Icon(Icons.cancel_rounded),
                            ),
                            SizedBox(
                              width: 105,
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: Icon(Icons.check_box),
                            )
                          ],
                        ),
                      ),
                      !_canshowButton
                          ? const SizedBox.shrink()
                          : FlatButton(
                              onPressed: () {
                                hideWidget();
                                setState(() {
                                  _isVisible = !_isVisible;
                                });
                              },
                              child: Center(
                                child: Row(
                                  children: [
                                    SizedBox(
                                      height: 120,
                                      width: 35,
                                    ),
                                    Icon(
                                      Icons.add,
                                      color: Colors.green,
                                    ),
                                    Text(
                                      "Add card",
                                      style: TextStyle(color: Colors.green),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                    ],
                  ),
                ],
              ),
            ),
            feedback: Container(
              height: 300,
              width: 200,
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                    blurRadius: 15.0,
                    offset: Offset(0.0, 4.0),
                    color: Colors.grey.withOpacity(0.50))
              ]),
            ),
          ),
          // DragTarget(builder: builder),
          SizedBox(
            width: 10,
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.grey.shade300,
                borderRadius: BorderRadius.all(Radius.circular(15))),
            height: 300,
            width: 200,
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("ON PROGRESS"),
                      IconButton(onPressed: () {}, icon: Icon(Icons.more_vert))
                    ],
                  ),
                  Container(
                    height: 200,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 25,
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: Icon(
                          Icons.add,
                          color: Colors.green,
                        ),
                      ),
                      Text(
                        "Add card",
                        style: TextStyle(color: Colors.green),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.grey.shade300,
                borderRadius: BorderRadius.all(Radius.circular(15))),
            height: 300,
            width: 200,
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("DONE"),
                      IconButton(onPressed: () {}, icon: Icon(Icons.more_vert))
                    ],
                  ),
                  Container(
                    height: 200,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 25,
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: Icon(
                          Icons.add,
                          color: Colors.green,
                        ),
                      ),
                      Text(
                        "Add card",
                        style: TextStyle(color: Colors.green),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
