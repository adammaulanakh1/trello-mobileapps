import 'package:flutter/material.dart';
import 'package:trelloapps/BoardScreen/Welcome.dart';
import 'package:trelloapps/RegistAndLogin/Login.dart';
import 'package:trelloapps/RegistAndLogin/Register.dart';

class Board extends StatefulWidget {
  // const Board({ Key? key }) : super(key: key);

  @override
  _BoardState createState() => _BoardState();
}

class _BoardState extends State<Board> {
  int _currentPage = 0;
  PageController _controller = PageController();

  List<Widget> _pages = [
    Welcome(
      title: "Get more done with Trello",
      desription:
          "Plan, track, and organize pretty much anything with anyone.\nPlan, track, and organize pretty much anything with anyone.",
      image: "images/screenFirst.png",
    ),
    Welcome(
      title: "Get organized",
      desription: "Plan, track, and organize pretty much anything with anyone.",
      image: "images/screenFirst.png",
    ),
    Welcome(
      title: "Track Service Provider",
      desription: "Plan, track, and organize pretty much anything with anyone.",
      image: "images/screenFirst.png",
    ),
    Welcome(
      title: "Track Service Provider",
      desription: "Plan, track, and organize pretty much anything with anyone.",
      image: "images/screenFirst.png",
    ),
  ];

  _onChanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(color: Colors.blue.shade600),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 70, left: 130),
              child: Image.asset(
                "images/trello_logo.png",
                height: 27,
              ),
            ),
          ),
          PageView.builder(
            scrollDirection: Axis.horizontal,
            controller: _controller,
            itemCount: _pages.length,
            onPageChanged: _onChanged,
            itemBuilder: (context, int index) {
              return _pages[index];
            },
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 1),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List<Widget>.generate(
                    _pages.length,
                    (int index) {
                      return AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        height: 10,
                        width: (index == _currentPage) ? 10 : 10,
                        margin: EdgeInsets.symmetric(
                          horizontal: 5,
                          vertical: 30,
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: (index == _currentPage)
                                ? Colors.white
                                : Colors.grey.withOpacity(0.9)),
                      );
                    },
                  ),
                ),
              ),
              RaisedButton(
                  child: Container(
                    height: 50,
                    width: 325,
                    child: Center(
                      child: Text(
                        "Get Started",
                        style: TextStyle(color: Colors.white, fontSize: 17),
                      ),
                    ),
                  ),
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(5.0),
                  ),
                  color: Colors.green,
                  onPressed: () {}),
              SizedBox(
                height: 15,
              ),
              RaisedButton(
                  child: Container(
                    height: 50,
                    width: 325,
                    child: Center(
                      child: Text(
                        "Log In",
                        style: TextStyle(color: Colors.white, fontSize: 17),
                      ),
                    ),
                  ),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0),
                      side: BorderSide(color: Colors.white)),
                  color: Colors.blue.shade600,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  }),
              SizedBox(
                height: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 50),
                    child: Row(
                      children: [
                        Text("By signing up, you agree to our ",
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                        Text("Terms of Service",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                                decoration: TextDecoration.underline)),
                        Text("and ",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            )),
                      ],
                    ),
                  ),
                  Text(
                    "Privacy Policy.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        decoration: TextDecoration.underline),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Contact support",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        decoration: TextDecoration.underline),
                  ),
                ],
              ),
              Row(
                children: [
                  GestureDetector(
                    onTap: () => {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => Register()))
                    },
                    child: Text(
                      "Sign in",
                      style: TextStyle(
                        color: Colors.blue,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 3,
              )
            ],
          ),
        ],
      ),
    );
  }
}
