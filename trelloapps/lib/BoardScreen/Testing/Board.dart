class BoardListObject {
  String title;
  List<BoardListObject> items;

  BoardListObject({this.items, this.title});
}

class BoardItemObject {
  String title;
  String from;

  BoardItemObject({this.title = '', this.from = ''});
}
