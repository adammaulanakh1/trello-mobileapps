import 'package:flutter/material.dart';
import 'package:trelloapps/Home/NavBar.dart';

class DragBoardCard extends StatefulWidget {
  // const DragBoardCard({ Key? key }) : super(key: key);

  @override
  _DragBoardCardState createState() => _DragBoardCardState();
}

class _DragBoardCardState extends State<DragBoardCard> {
  List<String> myCustomList = [
    "test 1",
    "test 2",
    "test 3",
    "ini 1",
    "ini 2",
    "ini 3"
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavBar(),
      appBar: AppBar(
        backgroundColor: Colors.blue.shade600,
        title: Text("Home"),
        actions: [
          Row(
            children: [
              IconButton(onPressed: () {}, icon: Icon(Icons.search)),
              IconButton(
                  icon: Icon(Icons.notifications_outlined), onPressed: () {}),
            ],
          )
        ],
      ),
      body: Center(
        child: ReorderableListView(
          children: List.generate(myCustomList.length, (index) {
            return ListTile(
              key: UniqueKey(),
              title: Text(myCustomList[index]),
            );
          }),
          onReorder: (int oldIndex, int newIndex) {
            setState(() {
              if (newIndex > oldIndex) {
                newIndex -= 1;
              }
              final String newString = myCustomList.removeAt(oldIndex);
              myCustomList.insert(newIndex, newString);
            });
          },
        ),
      ),
    );
  }
}
