import 'package:flutter/material.dart';

class CardScreen extends StatefulWidget {
  // const CardScreen({ Key? key }) : super(key: key);

  @override
  _CardScreenState createState() => _CardScreenState();
}

class _CardScreenState extends State<CardScreen> {
  bool accept = false;
  @override
  Widget build(BuildContext context) {
    Widget first = Container(
      width: 50,
      height: 50,
      color: Colors.red,
    );
    Widget second = Container(
      width: 50,
      height: 50,
      color: Colors.green,
    );
    Widget draggableStatus = Container(
      width: 50,
      height: 50,
      color: Colors.grey,
      child: accept == true ? Center(child: first) : null,
    );
    return Scaffold(
      appBar: AppBar(
        title: Text("Card"),
        actions: [
          IconButton(onPressed: () {}, icon: Icon(Icons.search)),
        ],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  accept == true
                      ? Container()
                      : Draggable(
                          axis: Axis.horizontal,
                          feedback: first,
                          child: first,
                          childWhenDragging: Container(
                            width: 50,
                          ),
                        ),
                  SizedBox(
                    width: 50,
                  ),
                  DragTarget(
                    builder: (context, data, rejectData) {
                      return draggableStatus;
                    },
                    onWillAccept: (data) {
                      return true;
                    },
                    onAccept: (data) {
                      setState(() {
                        accept = false;
                      });
                    },
                  )
                ],
              ),
              SizedBox(
                width: 10,
              ),
              Draggable(
                axis: Axis.horizontal,
                child: second,
                feedback: second,
                childWhenDragging: Container(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
