// import 'package:flutter/material.dart';

// class BoardTest extends StatefulWidget {
//   // const BoardTest({ Key? key }) : super(key: key);

//   @override
//   _BoardTestState createState() => _BoardTestState();
// }

// class _BoardTestState extends State<BoardTest> {
//   @override
//   Widget build(BuildContext context) {
//     double layoutHeight = MediaQuery.of(context).size.height;
//     double layoutWidth = MediaQuery.of(context).size.width;
//     return Scaffold(
//       body: Container(
//         height: layoutHeight,
//         width: layoutWidth,
//         child: Column(children: <Widget>[
//           Container(
//             height: layoutHeight * 0.2,
//             width: layoutWidth,
//             color: Colors.blue.shade600,
//             child: Center(
//               child: Container(
//                 child: Image.asset(
//                   "images/trello_logo.png",
//                   height: 50,
//                 ),
//               ),
//             ),
//           ),
//           Expanded(
//             child: Container(
//               color: Colors.red,
//             ),
//           )
//         ]),
//       ),
//     );
//   }
// }
