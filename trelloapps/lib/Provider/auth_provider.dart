import 'package:firebase_auth/firebase_auth.dart';

class AuthClass {
  FirebaseAuth auth = FirebaseAuth.instance;

  //Register
  Future<String> createAccount({String email, String password}) async {
    try {
      await auth.createUserWithEmailAndPassword(
          email: email, password: password);
      return "Email Sukses!";
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return 'The Password provider is too weak.';
      } else if (e.code == 'email-already-in-use') {
        return 'Email sudah digunakan.';
      }
    } catch (e) {
      return "Error occured";
    }
  }

  //Login
  Future<String> SignIn({String email, String password}) async {
    try {
      await auth.signInWithEmailAndPassword(email: email, password: password);
      return "Welcome";
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return 'No user found for that email.';
      } else if (e.code == 'wrong-password') {
        return 'Password Salah.';
      }
    }
  }

  //Reset Password
  Future<String> resetPassword({String email}) async {
    try {
      await auth.sendPasswordResetEmail(
        email: email,
      );
      return "Email sent";
    } catch (e) {
      return 'Error occured';
    }
  }

  //SignOut
  void signOut() {
    auth.signOut();
  }
}
